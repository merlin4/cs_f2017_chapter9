﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_WorkableDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee bob = new Employee("Bob");
            Dog spot = new Dog("Spot");
            Cat puff = new Cat("Puff");

            Console.WriteLine(
                "{0} says {1}", bob.Name, bob.Work()
            );
            Console.WriteLine(
                "{0} says {1}", spot.Name, spot.Work()
            );
            Console.WriteLine(
                "{0} says {1}", puff.Name, puff.Work()
            );
        }
    }
}
