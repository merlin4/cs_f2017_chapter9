﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_WorkableDemo
{
    public class Employee : IWorkable
    {
        public Employee(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }

        public virtual string Work()
        {
            return "I do my job";
        }
    }
}
