﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_WorkableDemo
{
    public class Cat : Animal
    {
        public Cat(string name) : base(name)
        {
        }

        public override string Speak()
        {
            return "meow";
        }

        public override string Work()
        {
            return "I watch mice";
        }
    }
}
