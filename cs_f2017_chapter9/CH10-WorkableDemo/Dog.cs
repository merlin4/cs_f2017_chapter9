﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_WorkableDemo
{
    public class Dog : Animal
    {
        public Dog(string name) : base(name)
        {
        }

        public override string Speak()
        {
            return "woof";
        }

        public override string Work()
        {
            return "I watch the house";
        }
    }
}
