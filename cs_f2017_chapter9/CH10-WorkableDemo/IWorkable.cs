﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_WorkableDemo
{
    public interface IWorkable
    {
        string Work();
    }
}
