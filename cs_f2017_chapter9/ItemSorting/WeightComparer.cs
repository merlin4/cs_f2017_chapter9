﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItemSorting
{
    class WeightComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            Item item1 = x as Item;
            Item item2 = y as Item;
            return -item1.Weight.CompareTo(item2.Weight);
        }
    }
}
