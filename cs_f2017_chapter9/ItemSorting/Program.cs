﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItemSorting
{
    class Program
    {
        static void Main(string[] args)
        {
            Item[] items = new Item[]
            {
                new Item { Id = 5, Name = "Apple", Weight = 0.5 },
                new Item { Id = 3, Name = "Orange", Weight = 0.9 },
                new Item { Id = 7, Name = "Pineapple", Weight = 0.3 },
            };

            Array.Sort(items);
            Display(items);
            Array.Sort(items, new NameComparer());
            Display(items);
            Array.Sort(items, new WeightComparer());
            Display(items);
        }

        static void Display(Item[] items)
        {
            foreach (Item item in items)
            {
                Console.WriteLine("{0}, {1}, {2}", item.Id, item.Name, item.Weight);
            }
            Console.WriteLine();
        }
    }
}
