﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItemSorting
{
    public class Item : IComparable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Weight { get; set; }

        public int CompareTo(object obj)
        {
            Item other = (Item)obj;
            return Id.CompareTo(other.Id);
        }
    }
}
