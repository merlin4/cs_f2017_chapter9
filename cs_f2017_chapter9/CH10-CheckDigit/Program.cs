﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_CheckDigit
{
    static class Program
    {
        static void Main(string[] args)
        {
            int accountNumber = 49;
            int revisedAccountNumber = accountNumber.AddCheckDigit();
            Console.WriteLine("Original {0}", accountNumber);
            Console.WriteLine("Revised {0}", revisedAccountNumber);
        }

        static int AddCheckDigit(this int num)
        {
            int check = (num / 10 + num % 10) % 10;
            return num * 10 + check;
        }
    }
}
