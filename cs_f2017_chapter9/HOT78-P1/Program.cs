﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOT78_P1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] values = new double[15];
            int count = PromptInput(values);

            double min, max, sum, avg;
            CalcStats(values, count, out min, out max, out sum, out avg);

            Console.WriteLine("min: {0}", min);
            Console.WriteLine("max: {0}", max);
            Console.WriteLine("avg: {0}", double.IsNaN(avg) ? "ERROR" : avg.ToString());
            Console.WriteLine("sum: {0}", sum);
            Console.WriteLine();

            for (int i = 0; i < count; ++i)
            {
                Console.Write(" " + values[i]);
            }
            Console.WriteLine();
        }

        static int PromptInput(double[] values)
        {
            int count = 0;
            for (; count < values.Length;)
            {
                Console.Write("Please enter a number or 'z' to quit: ");
                string input = Console.ReadLine();
                double num;

                if (input.ToLower() == "z")
                {
                    Console.WriteLine("Calculating Statistics");
                    Console.WriteLine();
                    break;
                }
                else if (double.TryParse(input, out num))
                {
                    values[count] = num;
                    ++count;
                }
                else
                {
                    Console.WriteLine("INVALID NUMBER");
                    Console.WriteLine();
                }
            }
            return count;
        }

        static void CalcStats(double[] values, int count, out double min, out double max, out double sum, out double avg)
        {
            min = values[0];
            max = values[0];
            sum = 0;

            for (int i = 0; i < count; ++i)
            {
                double value = values[i];

                sum += value;
                if (value < min) { min = value; }
                if (value > max) { max = value; }
            }

            avg = sum / count;
        }
    }
}
