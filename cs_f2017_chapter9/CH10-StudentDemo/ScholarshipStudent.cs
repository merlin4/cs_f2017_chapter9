﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_StudentDemo
{
    public class ScholarshipStudent : Student
    {
        public override int Credits
        {
            get { return _credits; }
            set
            {
                base.Credits = value;
                //_credits = value;
                _tuition = 0;
            }
        }

        //public override bool Equals(object obj)
        //{
        //    if (obj == null) { return false; }
        //    if (object.ReferenceEquals(this, obj)) { return true; }
        //    if (this.GetType() != obj.GetType()) { return false; }

        //    var other = (ScholarshipStudent)obj;
        //    return this._name == other._name
        //        && this._credits == other._credits;
        //}

        //public override int GetHashCode()
        //{
        //    return base.GetHashCode();
        //}
    }
}
