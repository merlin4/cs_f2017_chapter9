﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_StudentDemo
{
    public class Student
    {
        protected const double RATE = 55.75;
        protected string _name;
        protected int _credits;
        protected double _tuition;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual int Credits
        {
            get { return _credits; }
            set
            {
                _credits = value;
                _tuition = _credits * RATE;
            }
        }

        public double Tuition
        {
            get { return _tuition; }
        }

        public override string ToString()
        {
            return string.Format(
                "{0}'s tuition is {1:C}",
                Name, Tuition
            );
        }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            if (object.ReferenceEquals(this, obj)) { return true; }
            if (this.GetType() != obj.GetType()) { return false; }

            var other = (Student)obj;
            return this._name == other._name
                && this._credits == other._credits;
        }

        public override int GetHashCode()
        {
            return _name != null ? _name.GetHashCode() : 0;
        }
    }
}
