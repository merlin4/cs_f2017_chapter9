﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_StudentDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Student payingStudent = new Student();
            payingStudent.Name = "Megan";
            payingStudent.Credits = 15;

            //Console.WriteLine(
            //    "{0}'s tuition is {1:C}",
            //    payingStudent.Name,
            //    payingStudent.Tuition
            //);
            Console.WriteLine(payingStudent);

            Student freeStudent = new ScholarshipStudent();
            freeStudent.Name = "Luke";
            freeStudent.Credits = 15;

            //Console.WriteLine(
            //    "{0}'s tuition is {1:C}",
            //    freeStudent.Name,
            //    freeStudent.Tuition
            //);
            Console.WriteLine(freeStudent.ToString());
        }
    }
}
