﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferenceTableExample
{
    class Toy
    {
        public string Name { get; set; }
        public double Weight { get; set; }

        private int[] extra;

        public Toy(string name, double weight)
        {
            Name = name;
            Weight = weight;
            extra = new int[10000];
            Console.WriteLine("toy constructed: " + Name);
        }

        ~Toy()
        {
            Console.WriteLine("toy destructed: " + Name);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Test();
            Console.ReadLine();
            Console.WriteLine("end of Main()");
            Console.WriteLine();
        }

        static void Test()
        {
            Console.WriteLine("creating toys");
            Toy toy1 = new Toy("Red Wagon", 5);
            Toy toy2 = new Toy("Hotwheel", 0.1);
            Console.WriteLine();

            int toyCount = 100;
            Toy[] toyArray = new Toy[toyCount];
            for (int i = 0; i < toyCount; ++i)
            {
                toyArray[i] = new Toy("Teddy Bear " + i, 1);
                toy2 = new Toy("Hotwheel " + i, 0.1);
            }
            
            Console.WriteLine();
            Console.WriteLine("throwing away references");
            toy1 = null;
            toy2 = null;
            toyArray = new Toy[0];
            toyArray = null;
            //GC.Collect();
            
            Console.WriteLine("end of Test()");
        }
    }
}
