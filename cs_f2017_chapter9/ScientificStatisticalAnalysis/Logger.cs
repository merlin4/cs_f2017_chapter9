﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificStatisticalAnalysis
{
    public sealed class Logger
    {
        private static Logger _instance;

        public static Logger Instance
        {
            get { return _instance; }
        }

        static Logger()
        {
            Console.WriteLine("static constructor");
            _instance = new Logger();
        }

        private Logger()
        {
        }

        public void WriteLine(string message)
        {
            Console.WriteLine(message);
            //
            // ...Log message to file
        }
    }
}
