﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificStatisticalAnalysis
{
    public class DataPointAdvanced
    {
        private double _x;
        private double _y;
        private double _z;
        private string _label;
        private double _product;
        
        public DataPointAdvanced(double x = 0, double y = 0, double z = 0, string label = "")
        {
            _x = x;
            _y = y;
            _z = z;
            _label = label;
            _product = _x * _y * _z;
        }

        //public DataPoint(double x, double y, double z) : this(x, y, z, "")
        //{
        //    //_z = 9;
        //}

        //public DataPoint(double value) : this(value, value, value, "")
        //{
        //    //_y = 7;
        //}

        //public DataPoint() : this(0, 0, 0, "")
        //{
        //    //_x = 5;
        //}

        public void Display()
        {
            //DataPoint p = this;
            Console.WriteLine("{0}, {1}, {2}", _x, _y, _z);
        }

        // Accessor
        public double GetProduct()
        {
            return _product;
        }

        // Getter
        public double GetX()
        {
            return _x;
        }

        // Mutator
        public void SetAll(double value)
        {
            _x = value;
            _y = value;
            _z = value;
            _product = _x * _y * _z;
        }

        // Setter
        //public void SetX(double value)
        //{
        //    if (value >= 0)
        //    {
        //        _x = value;
        //    }
        //}

        public double x
        {
            get { return _x; }
            set {
                if (value >= 0)
                {
                    _x = value;
                    _product = _x * _y * _z;
                }
            }
        }

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
    }
}
