﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificStatisticalAnalysis
{
    public class DataSet
    {
        private readonly string _name;
        private int _count;
        private readonly DataSeries[] _series;

        public DataSet(int size, string name)
        {
            _name = name;
            _count = 0;
            _series = new DataSeries[size];
        }

        public string Name
        {
            get { return _name; }
            //set { _name = value; }
        }

        public int Count
        {
            get { return _count; }
        }

        public DataSeries this[int index]
        {
            get { return _series[index]; }
            set { _series[index] = value; }
        }

        public DataSeries GetSeries(int index)
        {
            return _series[index];
        }

        public bool Add(DataSeries item)
        {
            if (_count < _series.Length)
            {
                _series[_count] = item;
                ++_count;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void DisplayAll()
        {
            Console.WriteLine("DATASET: " + _name);
            for (int i = 0; i < _count; ++i)
            {
                _series[i].DisplayAll();
            }
            Console.WriteLine();
        }
    }
}
