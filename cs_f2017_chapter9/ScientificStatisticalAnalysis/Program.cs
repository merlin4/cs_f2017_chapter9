﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificStatisticalAnalysis
{
    class Program
    {
        private static DataSet _swoop = new DataSet(1000, "swoop");

        //public static void Add(DataSeries series)
        //{
        //    _swoop.Add(series);
        //}

        //public static DataSet swoop
        //{
        //    get { return _swoop; }
        //    set { _swoop = value; }
        //}

        static void Main(string[] args)
        {

            //_swoop = new DataSet(1000, "swoop");

            RandomDataGenerator random = new RandomDataGenerator(113);
            //DataSet set = random.GenerateSet("swoop");
            //set.DisplayAll();

            DataSeries series = random.GenerateSeries("swoop");
            series.DisplayAll();
            series.SortByX();
            series.DisplayAll();

            //series[0] = new DataPoint(1, 2, 3);
            //DataPoint p = series[0];
            //p.Display();

            DataPoint p0 = series.GetPoint(0);
            DataPoint p1 = series[1];
            DataPoint p2 = series[2];
            double product = DataPoint.DotProduct(p0, p1);
            Console.WriteLine();
            Console.WriteLine("DotProduct: {0}", product);

            //DataPoint sum1 = DataPoint.Add(DataPoint.Add(p0, p1), p2);
            //DataPoint sum2 = p0 + p1 + p2;
            //sum2.Display();

            {
                p0.Display();
                DataPoint neg = (-p0);
                p0.Display();
                neg.Display();
                Console.WriteLine(p0 == neg);
                Console.WriteLine(p0 != neg);
            }
            {
                DataPresenter.Display(p0);
                DataPoint neg = (-p0);
                DataPresenter.Display(p0);
                DataPresenter.Display(neg);
                Console.WriteLine(p0 == neg);
                Console.WriteLine(p0 != neg);
            }

            AxisId axis = AxisId.AXIS_X;
            axis.Display();
            Console.WriteLine(axis.GetDisplayName());

            Logger.Instance.WriteLine("App Started");
        }

        static int PromptPoints(DataPoint[] points)
        {
            int count = 0;
            while (count < points.Length)
            {
                double x, y, z;
                string input;

                Console.WriteLine();
                Console.Write("Enter x{0}: ", count);
                input = Console.ReadLine();
                if (!double.TryParse(input, out x)) { break; }

                Console.Write("Enter y{0}: ", count);
                input = Console.ReadLine();
                if (!double.TryParse(input, out y)) { break; }

                Console.Write("Enter z{0}: ", count);
                input = Console.ReadLine();
                if (!double.TryParse(input, out z)) { break; }

                points[count] = new DataPoint(x, y, z);
                ++count;
            }
            Console.WriteLine();
            return count;
        }

        static DataSeries PromptSeries()
        {
            Console.WriteLine("What do you want to name the series?");
            string name = Console.ReadLine();
            int size = 5;

            var series = new DataSeries(size, name);
            while (series.Count < size)
            {
                double x, y, z;
                string input;

                Console.WriteLine();
                Console.Write("Enter x{0}: ", series.Count);
                input = Console.ReadLine();
                if (!double.TryParse(input, out x)) { break; }

                Console.Write("Enter y{0}: ", series.Count);
                input = Console.ReadLine();
                if (!double.TryParse(input, out y)) { break; }

                Console.Write("Enter z{0}: ", series.Count);
                input = Console.ReadLine();
                if (!double.TryParse(input, out z)) { break; }

                series.Add(new DataPoint(x, y, z));
            }
            Console.WriteLine();
            return series;
        }
    }
}
