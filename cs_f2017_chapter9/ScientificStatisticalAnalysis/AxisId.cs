﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificStatisticalAnalysis
{
    public enum AxisId
    {
        AXIS_X, AXIS_Y, AXIS_Z
    }
}
