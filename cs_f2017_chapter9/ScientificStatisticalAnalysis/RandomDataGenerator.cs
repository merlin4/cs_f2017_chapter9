﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificStatisticalAnalysis
{
    public class RandomDataGenerator
    {
        private readonly Random _random;

        public RandomDataGenerator()
        {
            _random = new Random();
        }

        public RandomDataGenerator(int seed)
        {
            _random = new Random(seed);
        }

        public DataPoint GeneratePoint(string label = null)
        {
            var point = new DataPoint();
            point.x = _random.Next(-100, 100);
            point.y = _random.Next(-100, 100);
            point.z = _random.Next(-100, 100);
            point.Label = label;
            return point;
        }

        public DataSeries GenerateSeries(string name = null)
        {
            int size = _random.Next(3, 7);
            var series = new DataSeries(10, name);
            for (int i = 0; i < size; ++i)
            {
                series.Add(GeneratePoint("Point" + i));
            }
            return series;
        }

        public DataSet GenerateSet(string name = null)
        {
            int size = _random.Next(3, 5);
            var set = new DataSet(10, name);
            for (int i = 0; i < size; ++i)
            {
                set.Add(GenerateSeries("Series" + i));
            }
            return set;
        }

    }
}
