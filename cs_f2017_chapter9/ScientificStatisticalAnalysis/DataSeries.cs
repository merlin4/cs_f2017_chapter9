﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificStatisticalAnalysis
{
    public class DataSeries
    {
        private readonly string _name;
        private int _count;
        private readonly DataPoint[] _points;

        public DataSeries(int size, string name)
        {
            _name = name;
            _count = 0;
            _points = new DataPoint[size];
        }

        public string Name
        {
            get { return _name; }
            //set { _name = value; }
        }

        public int Count
        {
            get { return _count; }
        }

        public DataPoint this[int index]
        {
            get { return _points[index]; }
            set { _points[index] = value; }
        }

        public DataPoint GetPoint(int index)
        {
            return _points[index];
        }

        public bool Add(DataPoint item)
        {
            if (_count < _points.Length)
            {
                _points[_count] = item;
                ++_count;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void DisplayAll()
        {
            Console.WriteLine("DATASERIES: " + _name);
            for (int i = 0; i < _count; ++i)
            {
                Console.Write(" ({0}, {1}, {2})", _points[i].x, _points[i].y, _points[i].z);
            }
            Console.WriteLine();
        }

        public void SortByX()
        {
            Array.Sort(_points, 0, _count);
        }
    }
}
