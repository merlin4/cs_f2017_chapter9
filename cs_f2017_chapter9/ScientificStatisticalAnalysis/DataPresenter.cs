﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificStatisticalAnalysis
{
    public static class DataPresenter
    {
        //public static int MyStaticSwoop = 5;

        public static void Display(this DataPoint p)
        {
            Console.WriteLine("({0}, {1}, {2})", p.x, p.y, p.z);
        }

        public static int Sum(this int[] arr)
        {
            int sum = 0;
            for (int i = 0; i < arr.Length; ++i)
            {
                sum += arr[i];
            }
            return sum;
        }

        public static string GetDisplayName(this AxisId axis)
        {
            switch (axis)
            {
                case AxisId.AXIS_X: return "x";
                case AxisId.AXIS_Y: return "y";
                case AxisId.AXIS_Z: return "z";
                default: return "ERROR";
            }
        }

        public static void Display(this AxisId axis)
        {
            Console.WriteLine(GetDisplayName(axis));
        }
    }
}
