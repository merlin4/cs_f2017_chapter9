﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificStatisticalAnalysis
{
    public class DataPoint : IComparable
    {
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }
        public string Label { get; set; }

        public DataPoint(double x = 0, double y = 0, double z = 0, string label = "")
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.Label = label;
        }

        public double Product
        {
            get { return x * y * z; }
            //private set {
            //    //do thing 
            //}
        }

        //public void Display()
        //{
        //    Console.WriteLine("{0}, {1}, {2}", this.x, this.y, this.z);
        //}

        public int CompareTo(object obj)
        {
            DataPoint other = (DataPoint)obj;
            return x.CompareTo(other.x);
            //return Label.CompareTo(other.Label);
        }

        public static double DotProduct(DataPoint u, DataPoint v)
        {
            return u.x * v.x + u.y * v.y + u.z * v.z;
        }

        public static DataPoint Add(DataPoint u, DataPoint v)
        {
            return new DataPoint(u.x + v.x, u.y + v.y, u.z + v.z);
        }

        public static DataPoint operator +(DataPoint u, DataPoint v)
        {
            return new DataPoint(u.x + v.x, u.y + v.y, u.z + v.z);
        }

        public static DataPoint operator ++(DataPoint p)
        {
            p.x += 1;
            p.y += 1;
            p.z += 1;
            return p;
        }

        public static DataPoint operator -(DataPoint p)
        {
            //p.x = -p.x;
            //p.y = -p.y;
            //p.z = -p.z;
            return new DataPoint(-p.x, -p.y, -p.z);
        }

        public static bool operator !=(DataPoint u, DataPoint v)
        {
            return !(u == v);
            //return u.x != v.x || u.y != v.y || u.z != v.z;
        }

        public static bool operator ==(DataPoint u, DataPoint v)
        {
            //return !(u != v);
            //return u.x == v.x && 
            //       u.y == v.y && 
            //       u.z == v.z;
            
            return u.x == v.x
                && u.y == v.y
                && u.z == v.z;
        }

        //public static double operator +(DataPoint u, DataPoint v)
        //{
        //    return u.x + v.x;
        //}
    }
}
