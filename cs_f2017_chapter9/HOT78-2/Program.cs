﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOT78_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double grossPay = PromptGrossPay();

            for (;;)
            {
                Console.WriteLine("-----------------------------------------------------");
                Console.WriteLine("1: Display gross pay");
                Console.WriteLine("2: Display net pay with $100/year insurance deduction");
                Console.WriteLine("3: Display net pay with 30% tax");
                Console.WriteLine("4: Quit");
                Console.WriteLine("-----------------------------------------------------");

                string input = Console.ReadLine();
                double netPay;
                switch (input)
                {
                    case "1":
                    case "a":
                    case "A":
                        netPay = CalcNetPay(grossPay: grossPay);
                        Console.WriteLine("NET PAY: {0:C}", netPay);
                        break;
                    case "2":
                    case "b":
                    case "B":
                        netPay = CalcNetPay(grossPay: grossPay, insurance: 100);
                        Console.WriteLine("NET PAY: {0:C}", netPay);
                        break;
                    case "3":
                    case "c":
                    case "C":
                        netPay = CalcNetPay(grossPay: grossPay, tax: 0.3);
                        Console.WriteLine("NET PAY: {0:C}", netPay);
                        break;
                    case "4":
                    case "d":
                    case "D":
                        Console.WriteLine("HAVE A NICE DAY");
                        return;
                    default:
                        Console.WriteLine("INVALID MENU OPTION");
                        break;
                }
            }
        }

        static double PromptGrossPay()
        {
            Console.Write("What is your gross annual salary? ");
            string input = Console.ReadLine();
            return double.Parse(input);
        }

        static double CalcNetPay(double grossPay, double insurance = 0, double tax = 0)
        {
            return (grossPay - insurance) * (1 - tax);
        }
    }
}
