﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_AnimalDemo
{
    public abstract class Animal
    {
        protected readonly string _name;

        public Animal(string name)
        {
            _name = name;
        }

        public string Name
        {
            get { return _name; }
        }

        public abstract string Speak();
    }
}
