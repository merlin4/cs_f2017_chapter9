﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_AnimalDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal spot = new Dog("Spot");
            Animal puff = new Cat("Puff");
            DisplayAnimal(spot);
            DisplayAnimal(puff);
        }

        static void DisplayAnimal(Animal creature)
        {
            Console.WriteLine(
                "{0} says {1}", creature.Name, creature.Speak()
            );
        }
    }
}
