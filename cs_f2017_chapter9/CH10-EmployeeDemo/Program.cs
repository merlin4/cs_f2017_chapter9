﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_EmployeeDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee clerk = new Employee(123);
            //clerk.Id = 123;
            clerk.Salary = 30000.00;
            //clerk.CommissionRate = 0.07;

            Console.WriteLine();
            Console.WriteLine(clerk.GetGreeting());
            Console.WriteLine("Clerk #{0} salary: {1:C} per year", clerk.Id, clerk.Salary);

            CommissionEmployee salesPerson = new CommissionEmployee(234);
            //salesPerson.Id = 234;
            salesPerson.Salary = 20000.00;
            salesPerson.CommissionRate = 0.07;

            Console.WriteLine();
            Console.WriteLine(salesPerson.GetGreeting());
            Console.WriteLine("Salesperson #{0} salary: {1:C} per year", salesPerson.Id, salesPerson.Salary);
            Console.WriteLine("... plus {0:P} commission on all sales", salesPerson.CommissionRate);
        }
    }
}
