﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_EmployeeDemo
{
    public class Employee
    {
        protected int _id;
        protected double _salary;

        public Employee(int id)
        {
            Console.WriteLine("employee");
            _id = id;
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public double Salary
        {
            get { return _salary; }
            set { _salary = value; }
        }

        public virtual string GetGreeting()
        {
            return string.Format("Hello. I am employee #{0} and my salary is {1:C}", _id, _salary);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            if (object.ReferenceEquals(this, obj)) { return true; }
            if (this.GetType() != obj.GetType()) { return false; }

            var other = (Employee)obj;
            return this._id == other._id
                && this._salary == other._salary;
        }

        public override int GetHashCode()
        {
            return _id * 2 + 1;
        }
    }
}
