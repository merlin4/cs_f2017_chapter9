﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH10_EmployeeDemo
{
    public class CommissionEmployee : Employee
    {
        //protected double _salary;
        protected double _commissionRate;

        public CommissionEmployee(int id) : base(id)
        {
            Console.WriteLine("commission employee");
        }

        public double CommissionRate
        {
            get { return _commissionRate; }
            set
            {
                _commissionRate = value;
                //_salary = 0;
            }
        }

        public override string GetGreeting()
        {
            string greeting = base.GetGreeting();
            return greeting + " and I want my commision!";
        }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            if (object.ReferenceEquals(this, obj)) { return true; }
            if (this.GetType() != obj.GetType()) { return false; }

            var other = (CommissionEmployee)obj;
            return this._id == other._id
                && this._salary == other._salary
                && this._commissionRate == other._commissionRate;
        }

        public override int GetHashCode()
        {
            return _id * 3 + 7;
        }
    }
}
